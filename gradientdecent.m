function m= gradientdecent(class)
global W ;
global Y ;
global prev ;
global epsilon ;
global  iterations ;
global learn ;
cost = compute_cost(class);
i=0 ; 
disp('running gradientdecent for class ');
disp(class) ;
prev = -1000 ;
while(i<iterations)
    if rem(i,5)==0
        cost= compute_cost(class);
        disp(cost)
    end
    W(:,class)= W(:,class)-learn*gradient(class);   %%updating gradient  
    %%disp('iteration ')
    %%disp(cost)  
    %%prev = cost; 
    i=i+1 ;
end
 
 
    m=0; 


end