function cost = compute_cost(class)
global train ;
global lambda ;
global Y ;
global W ;
global X ;
temp1 = 0 ;
temp2 =0 ; 
vector = W(:,class) ;
for i = 1:train
    
    if Y(i) == class-1
    temp1 = temp1 +(-1.0)*log(1.0/(1.0+exp(-vector'*X(:,i)))) ;   
    else
    temp1= temp1 + (-1.0)*log(1.0-1.0/(1.0+exp(-vector'*X(:,i)))) ;    
    end
end

temp2= lambda*((W(:,class)'*W(:,class))-W(1,class)*W(1,class)) ;  %% subtraction of bias term 

cost = (temp1+temp2/2)/train ;

end