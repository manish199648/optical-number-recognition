%
%global W1 ;
%global W2 ;
%global X ;
%global Y ; 
%global a1; 
%global a2 ;
%global train ; 
%global learn; 
%global epsilon1 ;
%global epsilon2 ;
%global hidden_neuron ; 
%


derivative2 =zeros(10,hidden_neuron+1); 
derivative1= zeros(hidden_neuron+1,785) ;
sderivative1=zeros(hidden_neuron,785) ;
delta1 =zeros(10,train) ;
sum1=zeros(hidden_neuron,785) ;
sum2=zeros(10,hidden_neuron+1);

for i=1:train

     vec = zeros(10,1) ;
     vec(Y(i)+1) =1; 
    delta1 =-vec+a2(:,i); %%a2(:,i).*(1-a2(:,i)) ; %% delta for 1st layer

%% gradient for layer 2
               
    for j =1:hidden_neuron+1
        derivative2(:,j)= (delta1).*a1(j,i); 
    end

%% calculating delta for middle layer
temp_delta2= zeros(21,1); 
for l=1:hidden_neuron
    temp_delta2= W2'*delta1;
end
%%for z=1:hidden_neuron
    delta2 = temp_delta2; 
%%end
    %%gradient for layer 1
    res= a1(:,i).*(1-a1(:,i)) ;
for k= 1:785
   derivative1(:,k)=  res.*delta2.*X(k,i); 
end

for p=1:hidden_neuron 
sderivative1(p,:)= derivative1(p+1,:);  
end

sum1 = sum1+sderivative1; 
sum2= sum2+derivative2 ;

end

W1= W1 - learn*sum1/train ;
W2 =W2 - learn*sum2/train; 
