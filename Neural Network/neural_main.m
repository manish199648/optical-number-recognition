global W1 ;
global W2 ;
global X ;
global Y ; 
global a1; 
global a2 ;
global train ; 
global epsilon1 ;
global epsilon2 ;
global learn;
global hidden_neuron ;
global delta1 ;
global delta2 ;
global iterations ;
hidden_neuron =200 ;
iterations= 350 ;
learn = 0.3 ;
X = load('training_images.txt');  %% loading data
Y= load('training_labels.txt');
X =[ones(1,5000);X]; 

a1= ones(hidden_neuron,train) ;
a2= ones(10,train);                            
                    %%dimenions of W1 is 20*785
epsilon1 = (sqrt(6.0/(785+hidden_neuron)));
epsilon2 = (sqrt(6.0/hidden_neuron +10));
W1 = -epsilon1 + (2*epsilon1)*rand(hidden_neuron,785 );
W2 = -epsilon2 + (2*epsilon2)*rand(10,hidden_neuron+1);



%%forward propogation starts 
a1 = 1./(1.+exp(-W1*X));
a1= [ones(1,5000);a1];
a2 = 1./(1.+exp(-W2*a1));

back_prop(); 

a1 = 1./(1.+exp(-W1*X));
a1= [ones(1,5000);a1];
a2 = 1./(1.+exp(-W2*a1));

true = 0 ;
for i =1:5000
  
[M,I] = max(a2(:,i)) ;
if I-1==Y(i)
    true = true +1 ;
end
end

disp('accuracy %')
true*100/(5000)
