function m= cost()
global W1 ;
global W2 ;
global X ;
global Y ; 
global a1; 
global a2 ;
global train ; 
global epsilon1 ;
global epsilon2 ;
global learn;
global hidden_neuron ;
global delta1 ;
global delta2 ;
global iterations ;
m =0 ; 
for i=1:train
  for j=1:10
    if Y(i)==j-1
       m= m-log(a2(j,train)); 
    else
       m= m-log(1-a2(j,train));  
    end
  end
end

m=m/train ;


end