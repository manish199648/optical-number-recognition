function grad = gradient(class)
 
global  train ;
global X ;
global lambda ;
global Y ;
global W ;

vector= W(:,class); 

temp = zeros(785,1) ;
    for i= 1:train
        %%train  
        %%i
        if Y(i) == class-1
             temp = temp +(1.0/(1.0+exp(-vector'*X(:,i))) -1.0)*X(:,i) ;
        else
            temp = temp +(1.0/(1.0+exp(-vector'*X(:,i))) -0.0)*X(:,i);
        end
    end
    %%temp 
  grad= (temp)/train + (lambda/train)*W(:,class) ;    


end