global W ;
global Y ;
global lambda ;
global train ;
global iterations ;
global X ;
global learn ;
lambda =0.1 ;
learn = 0.001 ;
train =5000 ;
iterations = 500 ;
epsilon= 0.1 ;
X = load('training_images.txt');
%%X=X*255 ;
Y = load('training_labels.txt');
X =[ones(1,5000);X];
W = zeros(785,10);               %% one extra row for bias parameter 
for i=1:785
for j=1:10
    W(i,j) = 0 ; 
end
end

for K = 1:10
    gradientdecent(K) ;
    %%options = optimset('GradObj', 'on', 'MaxIter', 50);
    %%initial_theta = W(:,2);
    %%[all_theta(c,:)] = fmincg (@(t)(compute_cost(class)), initial_theta, options);
end


true = 0 ;
for i =1:5000
  
ans = 1./(1.+exp(-W.'*X(:,i))) ;
  
[M,I] = max(ans) ;
if I-1==Y(i)
    true = true +1 ;
end
end

disp('accuracy %')
true*100/(5000-train)